# Apprentissage Artificiel - Apprentissage d'un ensemble de règles - Algorithme de couverture itérative (Foil propositionnel)

<div align="center">
<img width="500" height="400" src="apprentissage.jpg">
</div>

## Description du projet

Application console réalisée avec Java SE en MASTER INFO 1 à l'université d'Angers dans le cadre du module "Apprentissage artificiel" durant l'année 2019-2020 avec un groupe de deux personnes. \
Le projet consiste à construire un ensemble de règles par l'algorithme de couverture itérative (Foil propositionnel).

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de deux étudiants de l'université d'Angers :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr ;
- Mohamed OUHIRRA : mouhirra@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par une enseignante de l'université d'Angers :
- Béatrice DUVAL : beatrice.duval@univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "Apprentissage artificiel" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2020 sur la période du confinement COVID-19 à la maison au mois de Mars-Avril. \
Il a été terminé et rendu le 06/04/2020.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Java ;
- Eclipse ;
- Maven ;
- Weka.

## Organisation
Le projet s'articule autour de Maven pour la gestion et l'automatisation de certaines taches (références projets, compilation, etc.).
Il est architecturé de la manière suivante :
- Les fichiers sources du programme principale (implémentation de l'algorithme FOIL) se trouve dans le dossier src/main/java.
- Les documents relatives au projet se situe dans le dossier src/main/resources. Ce dossier est subdivisé en plusieurs parties :
  - Un fichier "class_diagram.png" qui a pour but de visualiser les classes implémentées.
  - Un fichier "rapport.pdf" expliquant le programme.
  - Un dossier "data" qui contient les jeux de données en format ".arff", utilisés pour tester et valider le programme.
  - Un dossier "javadoc" contenant la documentation du code source du programme principal.
- Les tests ont été réalisés dans le dossier src/test/java.
- Enfin le programme .jar pour lancer l'application se trouve dans le dossier target.

## Lancement
Pour lancer l'application, il faut :
- Ouvrir un terminal.
- Se positionner dans le dossier du projet. Ex : cd ../Projet/project-foil-propositionnel.
- Générer le fichier exécutable .jar avec Maven. En effet je ne pouvais pas générer à l'avance car Moodle est limité à 20Mo maximum.
Le projet total fait environs 30Mo. De ce fait il faut effectuer cette étape supplémentaire en tapant la ligne de commande suivante :
mvn clean install
Un nouveau dossier "target" apparait
- Se placer dans le dossier target. Ex : cd ../Projet/project-foil-propositionnel/target
- Lancer la ligne de commande suivante : java -jar project-foil-propositionnel-0.0.1-SNAPSHOT-jar-with-dependencies.jar fichierArff.arff valeurPositive.\
En effet, en premier argument il faut lui fournir le chemin du fichier .arff dont on souhaite construire un ensemble de règles.\
Puis en second argument il faut lui donner la valeur de la classe cible permettant d'évaluer les instances qui sont positives.\
En effet l'algorithme se base sur des instances "négatives" et "positives". Donc pour évaluer quelles sont les instances qui sont positives et négatives, le programme se base sur cette valeur.\

Par exemple si on veut construire les règles pour savoir si on peut faire du sport basé sur les données météo, cela nous donne :\
java -jar project-foil-propositionnel-0.0.1-SNAPSHOT-jar-with-dependencies.jar ../src/main/resources/data/weather_nominal.arff yes

Cela nous affiche dans la console le résultat suivant :\
Données : ../src/main/resources/data/weather_nominal.arff\
Temps de résolution (en ms) : 63\
Les règles apprises :

Regle : 1
SI outlook = overcast
ALORS play = yes

Regle : 2
SI humidity = normal ET
SI windy = FALSE 
ALORS play = yes

Regle : 3
SI humidity = normal ET
SI outlook = sunny 
ALORS play = yes
package universite.angers.master.info.artificial.learning.foil.propositionnel;

import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import org.apache.log4j.Logger;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.DataSetWrapper;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.InstanceWrapper;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.Litteral;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.Rule;

/**
 * Construit un ensemble de règles par l'algorithme de couverture itérative (Foil propositionnel)
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class FoilPropositionnel {

	private static final Logger LOG = Logger.getLogger(FoilPropositionnel.class);

	/**
	 * Le jeu de données
	 */
	protected DataSetWrapper dataSet;

	/**
	 * L'ensemble des negatives
	 */
	protected List<InstanceWrapper> negatives;
	
	/**
	 * L'ensemble des positives Pos
	 */
	protected List<InstanceWrapper> positives;
	
	/**
	 * L'ensemble des positives Pos2
	 */
	protected List<InstanceWrapper> instancesPositives;
	
	/**
	 * L'ensemble des négatives Neg2
	 */
	protected List<InstanceWrapper> instancesNegatives;
	
	/**
	 * L'ensemble des conditions pour former une regle
	 */
	protected List<Litteral> conditionsRules;
	
	protected List<Litteral> litteraux;

	/**
	 * Construit un ensemble de règles par l'algorithme de couverture itérative (Foil propositionnel)
	 * @return l'ensemble des règles apprises
	 */
	public abstract List<Rule> solve(DataSetWrapper dataSet);

	/**
	 * Récupère le littéral qui maximise le gain entropie
	 * @return
	 */
	protected Litteral getLitteralMaximizeGain() {
		Queue<Litteral> candidates = this.getCandidates();
		LOG.debug("Candidates : " + candidates);

		//On prend le littéral en tete de liste
		return candidates.peek();
	}

	/**
	 * Retourne une liste de littéral candidat
	 * @return
	 */
	protected Queue<Litteral> getCandidates() {
		Queue<Litteral> candidates = new PriorityQueue<>();

		for (Litteral l : this.litteraux) {
			double gain = this.getGain(l);
			l.setGain(gain);
			LOG.debug("Gain : " + gain);

			candidates.add(l);
		}

		return candidates;
	}

	/**
	 * Calcul le gain entropie
	 * @param l
	 * @return
	 */
	protected abstract double getGain(Litteral l);

	/**
	 * Compte le nombre d'exemples positives qui satisfont le littéral l
	 * @param l
	 * @return
	 */
	protected int countInstancesPosSatisfy(Litteral l) {
		return this.countInstancesSatisfy(this.instancesPositives, l);
	}

	/**
	 * Compte le nombre d'exemples négatives qui satisfont le littéral l
	 * @param l
	 * @return
	 */
	protected int countInstancesNegSatisfy(Litteral l) {
		return this.countInstancesSatisfy(this.instancesNegatives, l);
	}

	/**
	 * Compte le nombre d'exemples dans un ensemble d'exemples qui satisfont le littéral l
	 * @param instances
	 * @param l
	 * @return
	 */
	protected int countInstancesSatisfy(List<InstanceWrapper> instances, Litteral l) {
		LOG.debug("Instances : " + instances);
		LOG.debug("Litteral : " + l);

		int count = 0;

		for (InstanceWrapper instance : instances) {
			String otherValue = instance.getValue(l.getAttribute());
			LOG.debug("Other value : " + otherValue);

			if (l.getValue().equals(otherValue))
				count++;
		}

		return count;
	}

	/**
	 * Supprime les exemples qui ne satisfont pas le littéral l
	 * @param instances
	 * @param l
	 */
	protected void removeInstancesNotSatisfy(List<InstanceWrapper> instances, Litteral l) {
		LOG.debug("Instances : " + instances);
		LOG.debug("Litteral : " + l);

		for (Iterator<InstanceWrapper> it = instances.iterator(); it.hasNext();) {
			InstanceWrapper instance = it.next();

			String otherValue = instance.getValue(l.getAttribute());
			LOG.debug("Other value : " + otherValue);

			if (!l.getValue().equals(otherValue))
				it.remove();
		}
	}
	
	/**
	 * Supprime les exemples qui satisfont le littéral l
	 * @param instances
	 * @param l
	 */
	protected void removeInstancesSatisfy(List<InstanceWrapper> instances, Litteral l) {
		LOG.debug("Instances : " + instances);
		LOG.debug("Litteral : " + l);

		for (Iterator<InstanceWrapper> it = instances.iterator(); it.hasNext();) {
			InstanceWrapper instance = it.next();

			String otherValue = instance.getValue(l.getAttribute());
			LOG.debug("Other value : " + otherValue);

			if (l.getValue().equals(otherValue))
				it.remove();
		}
	}
}

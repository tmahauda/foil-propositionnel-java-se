package universite.angers.master.info.artificial.learning.foil.propositionnel;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.DataSetWrapper;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.Litteral;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.Rule;

/**
 * A partir d’un ensemble d’exemples positifs et négatifs d’un concept à apprendre,
 * l’algorithme -que l’on appellera dans la suite FOILP1- construit un ensemble de
 * règles couvrant tous les positifs et ne couvrant aucun négatif.
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class FoilPropositionnelFOILP1 extends FoilPropositionnel {

	private static final Logger LOG = Logger.getLogger(FoilPropositionnelFOILP1.class);

	@Override
	public List<Rule> solve(DataSetWrapper dataSet) {
		this.dataSet = dataSet;
		LOG.debug("Data set : " + this.dataSet);
		
		//L'ensemble des règles apprives
		List<Rule> rules = new ArrayList<>();
		
		this.positives = new ArrayList<>(this.dataSet.getInstancesPositives());
		this.negatives = new ArrayList<>(this.dataSet.getInstancesNegatives());

		//Tant qu'il y a des positives = apprendre une regle
		while (!this.positives.isEmpty()) {
			this.conditionsRules = new ArrayList<>();
			this.instancesNegatives = new ArrayList<>(this.negatives);
			this.instancesPositives = new ArrayList<>(this.positives);
			this.litteraux = new ArrayList<>(this.dataSet.getLitteraux());

			Litteral l = null;
			
			//Tant qu'il y a des négatives = spécialiser la règle par l'ajout d'un littéral
			while (!this.instancesNegatives.isEmpty()) {
				// Choisir le littéral L qui maximise Gain(L, Pos2, Neg2)
				l = this.getLitteralMaximizeGain();
				LOG.debug("Litteral maximize gain : " + l);

				// Ajouter L à Conditions_Regle
				this.conditionsRules.add(l);

				// Retirer de Neg2 tous les exemples qui ne satisfont pas L
				this.removeInstancesNotSatisfy(this.instancesNegatives, l);

				// Retirer de Pos2 tous les exemples qui ne satisfont pas L
				this.removeInstancesNotSatisfy(this.instancesPositives, l);
				
				this.litteraux.remove(l);
			}

			//Ajouter à Règles la règle à partir des conditions
			rules.add(new Rule(this.conditionsRules, dataSet.getClasse()));
			
			//Retirer de Pos tous les exemples qui satisfont Conditions_Règle
			this.removeInstancesSatisfy(this.positives, l);
		}
		
		return rules;
	}
	
	/**
	 * Calcul le gain entropie
	 * @param l
	 * @return
	 */
	protected double getGain(Litteral l) {
		int P = this.instancesPositives.size();
		LOG.debug("P le nombre d’exemples dans Pos : " + P);

		int N = this.instancesNegatives.size();
		LOG.debug("N le nombre d’exemples dans Neg : " + N);

		int p = this.countInstancesPosSatisfy(l);
		l.setP(p);
		LOG.debug("p le nombre d’exemples dans Pos qui satisfont L : " + p);

		int n = this.countInstancesNegSatisfy(l);
		l.setN(n);
		LOG.debug("n le nombre d’exemples dans Neg qui satisfont L : " + n);

		// le gain n'est pas calculable pour p=0
		if (p == 0) return -1;
		
		double val1 = 0;
		if (P + N != 0)
			val1 = (double) P / (P + N);

		LOG.debug("val1 : " + val1);

		double val2 = 0;
		if (p + n != 0)
			val2 = (double) p / (p + n);

		LOG.debug("val2 : " + val2);

		double logGeneral = Math.log(val1) / Math.log(2.0);
		LOG.debug("log general : " + logGeneral);

		double logLitteral = Math.log(val2) / Math.log(2.0);
		LOG.debug("logLitteral : " + logLitteral);

		return p * (logLitteral - logGeneral);
	}
}

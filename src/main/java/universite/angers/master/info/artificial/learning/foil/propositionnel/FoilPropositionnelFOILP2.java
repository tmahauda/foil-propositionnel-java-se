package universite.angers.master.info.artificial.learning.foil.propositionnel;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.DataSetWrapper;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.InstanceWrapper;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.Litteral;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.Rule;

/**
 * Pour avoir un ensemble de règles couvrant tous les positifs et ne couvrant aucun
 * négatif, votre algorithme produit beaucoup de règles dont certaines sont très
 * spécialisées et couvrent peu d’exemples. On souhaite donc relâcher cette condition
 * afin que l’algorithme fournisse une solution plus réaliste.
 * En vous inspirant de ce que l’on a vu sur les autres méthodes de classification
 * supervisée (arbres de décision, forêts, ensemble de validation, ...) proposer une ou
 * plusieurs approches pour simplifier l’ensemble de règles obtenues.
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class FoilPropositionnelFOILP2 extends FoilPropositionnel {

	private static final Logger LOG = Logger.getLogger(FoilPropositionnelFOILP2.class);

	@Override
	public List<Rule> solve(DataSetWrapper dataSet) {
		this.dataSet = dataSet;
		LOG.debug("Data set : " + this.dataSet);
		
		//L'ensemble des règles apprives
		List<Rule> rules = new ArrayList<>();
		
		this.positives = new ArrayList<>(this.dataSet.getInstancesPositives());
		this.negatives = new ArrayList<>(this.dataSet.getInstancesNegatives());

		//Tant qu'il y a des positives = apprendre une regle
		while (!this.positives.isEmpty() && rules.size() < 10) {
			LOG.debug("Positives empty : " + this.positives.isEmpty());
			
			this.conditionsRules = new ArrayList<>();
			this.instancesNegatives = new ArrayList<>(this.negatives);
			this.instancesPositives = new ArrayList<>(this.positives);
			this.litteraux = new ArrayList<>(this.dataSet.getLitteraux());

			Litteral l = null;
			
			//Tant qu'il y a des négatives = spécialiser la règle par l'ajout d'un littéral
			while (!this.instancesNegatives.isEmpty() && this.conditionsRules.size() < 10) {
				LOG.debug("Instances négatives : " + this.instancesNegatives.isEmpty());
				
				// Choisir le littéral L qui maximise Gain(L, Pos2, Neg2)
				l = this.getLitteralMaximizeGain();
				LOG.debug("Litteral maximize gain : " + l);

				// Ajouter L à Conditions_Regle
				this.conditionsRules.add(l);

				// Retirer de Neg2 tous les exemples qui ne satisfont pas L
				this.removeInstancesNotSatisfy(this.instancesNegatives, l);

				// Retirer de Pos2 tous les exemples qui ne satisfont pas L
				this.removeInstancesNotSatisfy(this.instancesPositives, l);
				
				this.litteraux.remove(l);
			}

			//Ajouter à Règles la règle à partir des conditions
			rules.add(new Rule(this.conditionsRules, dataSet.getClasse()));
			
			//Retirer de Pos tous les exemples qui satisfont Conditions_Règle
			this.removeInstancesSatisfy(this.positives, l);
		}
		
		return rules;
		
//		FoilPropositionnelFOILP1 foilp1 = new FoilPropositionnelFOILP1();
//		
//		List<Rule> rules = foilp1.solve(dataSet);
//		LOG.debug("Rules : " + rules);
//
//		//On effectue les tests sur l'ensembles des tests
//		this.dataSet = FactoryDataSet.buildTest(this.dataSet.getFilename(), this.dataSet.getEval());
//		
//		for(Rule rule : rules) {
//			this.propInstancesPosCheckRule(rule);
//			this.propInstancesNegCheckRule(rule);
//		}
//		
//		for(Rule rule : rules) {
//			LOG.debug("Rule : " + rule);
//			LOG.debug("Count pos : " + rule.getCountInstancesPosCheckRule());
//			LOG.debug("Count neg : " + rule.getCountInstancesNegCheckRule());
//			LOG.debug("Prop pos : " + rule.getPropInstancesPosCheckRule());
//			LOG.debug("Prop neg : " + rule.getPropInstancesNegCheckRule());
//		}
//		
//		int instancesPosCheckAllRule = this.countInstancesCheckAllRule(this.dataSet.getInstancesPositives(), rules);
//		LOG.debug("Instances pos check all rule : " + instancesPosCheckAllRule);
//		
//		int instancesNegCheckAllRule = this.countInstancesCheckAllRule(this.dataSet.getInstancesNegatives(), rules);
//		LOG.debug("Instances neg check all rule : " + instancesNegCheckAllRule);
//		
//		return rules;
	}
	
	/**
	 * Calcul le ratio
	 * @param l
	 * @return
	 */
	protected double getGain(Litteral l) {
		int countInstancesPositives = this.instancesPositives.size();
		int countInstancesNegatives = this.instancesNegatives.size();
		
		//True Positive
		int tp = this.countInstancesSatisfy(this.instancesPositives, l);
		
		//Faux positive
		int fp = this.countInstancesSatisfy(this.instancesNegatives, l);
		
		double generecity = (double)tp / (double)countInstancesPositives;
		double accuracy = (double)tp/((double)tp+(double)fp);
		double coverage = ((double)tp+(double)fp)/(double)this.dataSet.getInstances().size();
		
	    return generecity;
	}
	
	/**
	 * Nombre d'instances qui vérifie toutes les règles
	 */
	private int countInstancesCheckAllRule(List<InstanceWrapper> instances, List<Rule> rules) {
		int count = 0;
		
		for(InstanceWrapper instance : instances) {
			
			for(Rule rule : rules) {
				//Si au moins une règle est respecté
				if(rule.isRespect(instance)) {
					count++;
					break;
				}
			}
		}
		
		return count;
	}
	
	/**
	 * Proportion de positives classés correctement qui vérifie la règle
	 * @param rule
	 * @return
	 */
	private void propInstancesPosCheckRule(Rule rule) {
		int count = this.countInstancesCheckRule(this.dataSet.getInstancesPositives(), rule);
		rule.setCountInstancesPosCheckRule(count);
		
		double prop = (double)count / (double)this.dataSet.getInstancesPositives().size();
		LOG.debug("Proportion d'instances positives qui vérifie la règle : " + prop);	
		
		rule.setPropInstancesPosCheckRule(prop);
	}
	
	/**
	 * Proportion de négatives classés correctement qui vérifie la règle
	 * @param rule
	 * @return
	 */
	private void propInstancesNegCheckRule(Rule rule) {
		int count = this.countInstancesCheckRule(this.dataSet.getInstancesNegatives(), rule);
		rule.setCountInstancesNegCheckRule(count);
		
		double prop = (double)count / (double)this.dataSet.getInstancesNegatives().size();
		LOG.debug("Proportion d'instances négatives qui vérifie la règle : " + prop);	
		
		rule.setPropInstancesNegCheckRule(prop);
	}
	
	/**
	 * Nombre d'instances classés correctement qui vérifie la règle
	 * @param rule
	 * @return
	 */
	private int countInstancesCheckRule(List<InstanceWrapper> instances, Rule rule) {
		int count = 0;
		
		for(InstanceWrapper instance : instances) {
			//Si l'instance vérifie la regle
			if(rule.isRespect(instance)) count++;
		}
		LOG.debug("Nombre d'instances qui couvre la règle : " + count);
		
		return count;
	}
}
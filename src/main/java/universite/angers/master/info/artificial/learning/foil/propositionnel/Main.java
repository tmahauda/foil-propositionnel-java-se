package universite.angers.master.info.artificial.learning.foil.propositionnel;

import java.util.List;
import org.apache.log4j.Logger;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.DataSetWrapper;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.Evaluable;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.FactoryDataSet;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.InstanceWrapper;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.Rule;

/**
 * Main qui permet d'éxécuter le programme
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class Main {

	private static final Logger LOG = Logger.getLogger(Main.class);
	
	public static void main(String[] args) {
		if(args.length != 2) {
			System.out.println("Vous devez fournir : ");
			System.out.println("En premier argument : le chemin du fichier au format .arff contenant le jeu de données");
			System.out.println("En second argument : la valeur de la classe cible permettant d'évaluer les instances positives");
			return;
		}
		
		String fileName = args[0];
		LOG.debug("File name : " + fileName);
		
		String valuePos = args[1];
		LOG.debug("Value pos: " + valuePos);
		
		boolean find = findRules(new FoilPropositionnelFOILP1(), fileName, valuePos);
		if(!find) {
			System.out.println("Impossible de construire un ensemble de règles selon le jeu de données fournis");
		}
	}
	
	/**
	 * Trouver un ensemble de règles d'un jeu de données selon la stratégie de résolution
	 * @param fp la stratégie de résolution
	 * @param filename le jeu de données
	 * @param valuePositive la valeur cible permettant d'évaluer les instances positives
	 * @return
	 */
	private static boolean findRules(FoilPropositionnel fp, String filename, String valuePositive) {
		DataSetWrapper dataSet = FactoryDataSet.buildAllData(filename, new Evaluable() {
			@Override
			public boolean isPositive(InstanceWrapper instance) {
				String value = instance.getValueClasse();
				LOG.debug("Value : " + value);
				
				return valuePositive.equals(value);
			}
		});
		
		if(dataSet == null) return false;
		
		long start = System.currentTimeMillis();
		List<Rule> rules = fp.solve(dataSet);
		long end = System.currentTimeMillis();
		LOG.debug("Rules : " + rules);
		
		System.out.println("Données : " + filename);
		System.out.println("Temps de résolution (en ms) : " + (end-start));
		System.out.println("Les règles apprises : ");
		int i = 1;
		
		for(Rule rule : rules) {
			System.out.println();
			System.out.println("Regle : " + i);
			System.out.println(rule);
			System.out.println("ALORS " + dataSet.getData().classAttribute().name() + " = " + valuePositive);
			i++;
		}
		
		return true;
	}

}

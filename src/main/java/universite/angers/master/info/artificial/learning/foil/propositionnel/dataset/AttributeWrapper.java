package universite.angers.master.info.artificial.learning.foil.propositionnel.dataset;

import org.apache.log4j.Logger;
import weka.core.Attribute;

/**
 * Enveloppe un attribut d'instance (exemple) de weka afin d'offrir des fonctionnalités supplémentaires
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class AttributeWrapper {

	private static final Logger LOG = Logger.getLogger(AttributeWrapper.class);
	
	/**
	 * L'attribut enveloppé
	 */
	protected Attribute attribute;
	
	public AttributeWrapper(Attribute attribute) {
		this.attribute = attribute;
		LOG.debug("Attribute : " + this.attribute);
	}

	/**
	 * @return the attribute
	 */
	public Attribute getAttribute() {
		return attribute;
	}

	/**
	 * @param attribute the attribute to set
	 */
	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attribute == null) ? 0 : attribute.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttributeWrapper other = (AttributeWrapper) obj;
		if (attribute == null) {
			if (other.attribute != null)
				return false;
		} else if (!attribute.equals(other.attribute))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AttributeWrapper [attribute=" + attribute + "]";
	}
}

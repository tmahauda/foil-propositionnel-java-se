package universite.angers.master.info.artificial.learning.foil.propositionnel.dataset;

import weka.core.Attribute;

/**
 * Enveloppe une classe d'instance (exemple) de weka afin d'offrir des fonctionnalités supplémentaires
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClasseWrapper extends AttributeWrapper {

	public ClasseWrapper(Attribute attribute) {
		super(attribute);
	}
}

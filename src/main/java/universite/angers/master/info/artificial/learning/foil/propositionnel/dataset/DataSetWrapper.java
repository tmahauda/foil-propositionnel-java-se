package universite.angers.master.info.artificial.learning.foil.propositionnel.dataset;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import org.apache.log4j.Logger;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Enveloppe un jeu de données lu depuis weka afin d'offrir des fonctionnalités supplémentaires
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class DataSetWrapper {
	
	private static final Logger LOG = Logger.getLogger(DataSetWrapper.class);
	
	/**
	 * Le fichier qui contient l'ensembles des jeux de données
	 */
	private String filename;
	
	/**
	 * Le jeu de données
	 */
	private Instances data;
	
	/**
	 * La fonction d'évaluation
	 */
	private Evaluable eval;
	
	/**
	 * Les littéraux
	 */
	private List<Litteral> litteraux;
	
	/**
	 * Les exemples
	 */
	private List<InstanceWrapper> instances;
	
	/**
	 * Les exemples positives
	 */
	private List<InstanceWrapper> instancesPositives;
	
	/**
	 * Les exemples négatives
	 */
	private List<InstanceWrapper> instancesNegatives;
	
	/**
	 * La classe cible
	 */
	private ClasseWrapper classe;
	
	/**
	 * Constructeur qui prend le jeu données lu depuis Weka et la fonction d'évaluation
	 * permettant d'évaluer si un exemple est positive ou non
	 * @param data le jeu de données
	 * @param eval la fonction d'évaluation qui permet de savoir si un exemple est positive ou non
	 */
	public DataSetWrapper(String filename, Instances data, Evaluable eval) {
		this.filename = filename;
		LOG.debug("File name : " + this.filename);
		
		this.data = data;
		LOG.debug("Data set : " + this.data);
		
		this.eval = eval;
		LOG.debug("Eval : " + this.eval);
		
		this.classe = new ClasseWrapper(this.data.classAttribute());
		
		this.litteraux = this.buildLitteraux();
		LOG.debug("Litteraux : " + this.litteraux);
		LOG.debug("Size litteraux : " + this.litteraux.size());
		
		this.instances = this.buildInstances(eval);
		LOG.debug("Instances : " + this.instances);
		LOG.debug("Size instances : " + this.instances.size());
		
		this.instancesPositives = this.buildInstancesPositives();
		LOG.debug("Instances positives : " + this.instancesPositives);
		LOG.debug("Size positives : " + this.instancesPositives.size());
		
		this.instancesNegatives = this.buildInstancesNegatives();
		LOG.debug("Instances negatives : " + this.instancesNegatives);
		LOG.debug("Size negatives : " + this.instancesNegatives.size());
	}
	
	/**
	 * Construit les instances depuis le jeu de données
	 * @param eval
	 * @return
	 */
	private List<InstanceWrapper> buildInstances(Evaluable eval) {
		List<InstanceWrapper> instances = new ArrayList<>();
		
		Enumeration<Instance> enumInstances = this.data.enumerateInstances();
		while(enumInstances.hasMoreElements()) {
			Instance instance = enumInstances.nextElement();
			InstanceWrapper wrapper = new InstanceWrapper(instance, this.classe, eval);
			
			instances.add(wrapper);
		}
		
		return instances;
	}
	
	/**
	 * Construit les instances positives avec le jeu de données
	 * @return
	 */
	private List<InstanceWrapper> buildInstancesPositives() {
		List<InstanceWrapper> instancesPositives = new ArrayList<>();
		
		for(InstanceWrapper instance : this.instances) {
			if(instance.isPositive()) {
				instancesPositives.add(instance);
			}
		}
		
		return instancesPositives;
	}
	
	/**
	 * Construit les instances négatives avec le jeu de données
	 * @return
	 */
	private List<InstanceWrapper> buildInstancesNegatives() {
		List<InstanceWrapper> instancesNegatives = new ArrayList<>();
		
		for(InstanceWrapper instance : this.instances) {
			if(!instance.isPositive()) {
				instancesNegatives.add(instance);
			}
		}
		
		return instancesNegatives;
	}
	
	/**
	 * Construit les littéraux avec le jeu de données
	 * @return
	 */
	private List<Litteral> buildLitteraux() {
		List<Litteral> litteraux = new ArrayList<>();
		
		//Pour chaque attribut
		Enumeration<Attribute> enumAttributes = this.data.enumerateAttributes();
		while(enumAttributes.hasMoreElements()) {
			Attribute attr = enumAttributes.nextElement();
			
			//Pour chaque valeur de l'attribut
			for(int i=0; i<attr.numValues(); i++) {
				litteraux.add(new Litteral(attr, attr.value(i)));
			}
		}
		
		return litteraux;
	}
	
	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * @return the data
	 */
	public Instances getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Instances data) {
		this.data = data;
	}

	/**
	 * @return the litteraux
	 */
	public List<Litteral> getLitteraux() {
		return litteraux;
	}

	/**
	 * @param litteraux the litteraux to set
	 */
	public void setLitteraux(List<Litteral> litteraux) {
		this.litteraux = litteraux;
	}

	/**
	 * @return the instances
	 */
	public List<InstanceWrapper> getInstances() {
		return instances;
	}

	/**
	 * @param instances the instances to set
	 */
	public void setInstances(List<InstanceWrapper> instances) {
		this.instances = instances;
	}

	/**
	 * @return the instancesPositives
	 */
	public List<InstanceWrapper> getInstancesPositives() {
		return instancesPositives;
	}

	/**
	 * @param instancesPositives the instancesPositives to set
	 */
	public void setInstancesPositives(List<InstanceWrapper> instancesPositives) {
		this.instancesPositives = instancesPositives;
	}

	/**
	 * @return the instancesNegatives
	 */
	public List<InstanceWrapper> getInstancesNegatives() {
		return instancesNegatives;
	}

	/**
	 * @param instancesNegatives the instancesNegatives to set
	 */
	public void setInstancesNegatives(List<InstanceWrapper> instancesNegatives) {
		this.instancesNegatives = instancesNegatives;
	}
	
	/**
	 * @return the classe
	 */
	public ClasseWrapper getClasse() {
		return classe;
	}

	/**
	 * @param classe the classe to set
	 */
	public void setClasse(ClasseWrapper classe) {
		this.classe = classe;
	}
	
	/**
	 * @return the eval
	 */
	public Evaluable getEval() {
		return eval;
	}

	/**
	 * @param eval the eval to set
	 */
	public void setEval(Evaluable eval) {
		this.eval = eval;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((instances == null) ? 0 : instances.hashCode());
		result = prime * result + ((instancesNegatives == null) ? 0 : instancesNegatives.hashCode());
		result = prime * result + ((instancesPositives == null) ? 0 : instancesPositives.hashCode());
		result = prime * result + ((litteraux == null) ? 0 : litteraux.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataSetWrapper other = (DataSetWrapper) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (instances == null) {
			if (other.instances != null)
				return false;
		} else if (!instances.equals(other.instances))
			return false;
		if (instancesNegatives == null) {
			if (other.instancesNegatives != null)
				return false;
		} else if (!instancesNegatives.equals(other.instancesNegatives))
			return false;
		if (instancesPositives == null) {
			if (other.instancesPositives != null)
				return false;
		} else if (!instancesPositives.equals(other.instancesPositives))
			return false;
		if (litteraux == null) {
			if (other.litteraux != null)
				return false;
		} else if (!litteraux.equals(other.litteraux))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DataSetWrapper [data=" + data + ", litteraux=" + litteraux + ", instances=" + instances
				+ ", instancesPositives=" + instancesPositives + ", instancesNegatives=" + instancesNegatives + "]";
	}
}

package universite.angers.master.info.artificial.learning.foil.propositionnel.dataset;

/**
 * Interface qui permet d'évaluer si une instance est positive ou non
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Evaluable {

	/**
	 * Vérifier si une instance est positive ou non
	 * @param instance
	 * @return
	 */
	public boolean isPositive(InstanceWrapper instance);
}

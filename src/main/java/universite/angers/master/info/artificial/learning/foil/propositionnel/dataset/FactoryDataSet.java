package universite.angers.master.info.artificial.learning.foil.propositionnel.dataset;

import java.io.File;
import org.apache.log4j.Logger;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

/**
 * Construit le jeu de données à partir de l'API Weka
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class FactoryDataSet {

	private static final Logger LOG = Logger.getLogger(FactoryDataSet.class);
	
	private FactoryDataSet() {
		
	}
	
	private static Instances build(String filename) {
		LOG.debug("Build data set : " + filename);
		
		try {
			//On charge le fichier dans le loader
			ArffLoader loader = new ArffLoader();
			loader.setFile(new File(filename));
			loader.getStructure();
			
			//On récupère le jeux de données
			Instances data = loader.getDataSet();
			
			// Make the last attribute be the class 
			data.setClassIndex(data.numAttributes() - 1);	
			
			return data;
		}
		catch(Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}
	
	/**
	 * Récupère tous le jeux de données d'un fichier
	 * @param filename
	 * @param eval
	 * @return
	 */
	public static DataSetWrapper buildAllData(String filename, Evaluable eval) {
		Instances data = build(filename);
		if(data == null) return null;

		return new DataSetWrapper(filename, data, eval);
	}
	
	/**
	 * Sélectionne que 70% des données pour l'apprentissage
	 * @param filename
	 * @param eval
	 * @return
	 */
	public static DataSetWrapper buildApprentissage(String filename, Evaluable eval) {
		Instances data = build(filename);
		if(data == null) return null;
		
		int start = (70 * data.size())/100;
		int end = data.size() - 1;
		
		//On supprime les 30% données restantes pour n'avoir que 70%
		removeData(data, start, end);
		
		return new DataSetWrapper(filename, data, eval);
	}
	
	/**
	 * Sélectionne que 30% des données pour les tests
	 * @param filename
	 * @param eval
	 * @return
	 */
	public static DataSetWrapper buildTest(String filename, Evaluable eval) {
		Instances data = build(filename);
		if(data == null) return null;
		
		int start = 0;
		int end = (70 * data.size())/100;
		
		//On supprime les 70% données pour n'avoir que 30%
		removeData(data, start, end);
		
		return new DataSetWrapper(filename, data, eval);
	}
	
	/**
	 * Suppression des données
	 * @param data
	 * @param start
	 * @param end
	 */
	private static void removeData(Instances data, int start, int end) {
		LOG.debug("Start : " + start);
		LOG.debug("End : " + end);
		
		// it's important to iterate from last to first, because when we remove
		// an instance, the rest shifts by one position.
		for (int i = end; i >= start; i--) {
			data.delete(i);
		}
	}
}

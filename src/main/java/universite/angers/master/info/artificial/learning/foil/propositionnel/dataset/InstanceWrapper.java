package universite.angers.master.info.artificial.learning.foil.propositionnel.dataset;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import org.apache.log4j.Logger;
import weka.core.Attribute;
import weka.core.Instance;

/**
 * Enveloppe une instance (exemple) de weka afin d'offrir des fonctionnalités supplémentaires
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class InstanceWrapper {

	private static final Logger LOG = Logger.getLogger(InstanceWrapper.class);
	
	/**
	 * L'instance Weka
	 */
	private Instance instance;
	
	/**
	 * Les attributs de l'instance
	 */
	private List<AttributeWrapper> attributes;
	
	/**
	 * La classe de l'instance
	 */
	private ClasseWrapper classe;
	
	/**
	 * La fonction d'évaluation
	 */
	private Evaluable eval;
	
	public InstanceWrapper(Instance instance, ClasseWrapper classe, Evaluable eval) {		
		this.instance = instance;
		LOG.debug("Instance : " + this.instance);
		
		this.classe = classe;
		LOG.debug("Classe : " + this.classe);
		
		this.eval = eval;
		LOG.debug("Eval : " + this.eval);
		
		this.attributes = this.buildAttributes();
		LOG.debug("Attributes : " + this.attributes);
		LOG.debug("Size attributes : " + this.attributes.size());
	}
	
	/**
	 * Construit les attributs depuis l'instance weka
	 * @param eval
	 * @return
	 */
	private List<AttributeWrapper> buildAttributes() {
		List<AttributeWrapper> attributes = new ArrayList<>();
		
		Enumeration<Attribute> enumAttributes = instance.enumerateAttributes();
		while(enumAttributes.hasMoreElements()) {
			Attribute attribute = enumAttributes.nextElement();
			AttributeWrapper wrapper = new AttributeWrapper(attribute);
			
			attributes.add(wrapper);
		}
		
		return attributes;
	}
	
	/**
	 * Récupère la valeur de la classe
	 * @return
	 */
	public String getValueClasse() {
		return this.getValue(this.classe.getAttribute());
	}
	
	/**
	 * Récupère la valeur d'un attribut
	 * @param a
	 * @return
	 */
	public String getValue(Attribute a) {
		return this.instance.stringValue(a);
	}
	
	/**
	 * Savoir si un exemple est positive ou non
	 * @return
	 */
	public boolean isPositive() {
		return this.eval.isPositive(this);
	}

	/**
	 * @return the instance
	 */
	public Instance getInstance() {
		return instance;
	}

	/**
	 * @param instance the instance to set
	 */
	public void setInstance(Instance instance) {
		this.instance = instance;
	}

	/**
	 * @return the attributes
	 */
	public List<AttributeWrapper> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(List<AttributeWrapper> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @return the classe
	 */
	public ClasseWrapper getClasse() {
		return classe;
	}

	/**
	 * @param classe the classe to set
	 */
	public void setClasse(ClasseWrapper classe) {
		this.classe = classe;
	}

	/**
	 * @return the eval
	 */
	public Evaluable getEval() {
		return eval;
	}

	/**
	 * @param eval the eval to set
	 */
	public void setEval(Evaluable eval) {
		this.eval = eval;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attributes == null) ? 0 : attributes.hashCode());
		result = prime * result + ((classe == null) ? 0 : classe.hashCode());
		result = prime * result + ((eval == null) ? 0 : eval.hashCode());
		result = prime * result + ((instance == null) ? 0 : instance.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InstanceWrapper other = (InstanceWrapper) obj;
		if (attributes == null) {
			if (other.attributes != null)
				return false;
		} else if (!attributes.equals(other.attributes))
			return false;
		if (classe == null) {
			if (other.classe != null)
				return false;
		} else if (!classe.equals(other.classe))
			return false;
		if (eval == null) {
			if (other.eval != null)
				return false;
		} else if (!eval.equals(other.eval))
			return false;
		if (instance == null) {
			if (other.instance != null)
				return false;
		} else if (!instance.equals(other.instance))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InstanceWrapper [instance=" + instance + ", attributes=" + attributes + ", classe=" + classe + ", eval="
				+ eval + "]";
	}
}

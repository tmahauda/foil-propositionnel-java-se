package universite.angers.master.info.artificial.learning.foil.propositionnel.dataset;

import weka.core.Attribute;

/**
 * Le littéral composé de l'attribut avec sa valeur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class Litteral implements Comparable<Litteral> {

	/**
	 * L'attribut Weka
	 */
	private Attribute attribute;
	
	/**
	 * La valeur de l'attribut
	 */
	private String value;
	
	/**
	 * Le nombre d’exemples dans Pos qui satisfont ce littéral
	 */
	private int p;
	
	/**
	 * Le nombre d’exemples dans Neg qui satisfont ce littéral
	 */
	private int n;
	
	/**
	 * Gain entropie. Voir https://fr.wikipedia.org/wiki/Entropie_de_Shannon
	 */
	private double gain;
	
	public Litteral(Attribute attribute, String value) {
		this.attribute = attribute;
		this.value = value;
		this.p = 0;
		this.n = 0;
		this.gain = 0;
	}

	@Override
	public int compareTo(Litteral l) {
		return Double.compare(l.gain, this.gain);
	}
	
	/**
	 * @return the attribute
	 */
	public Attribute getAttribute() {
		return attribute;
	}

	/**
	 * @param attribute the attribute to set
	 */
	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the gain
	 */
	public double getGain() {
		return gain;
	}

	/**
	 * @param gain the gain to set
	 */
	public void setGain(double gain) {
		this.gain = gain;
	}

	/**
	 * @return the p
	 */
	public int getP() {
		return p;
	}

	/**
	 * @param p the p to set
	 */
	public void setP(int p) {
		this.p = p;
	}

	/**
	 * @return the n
	 */
	public int getN() {
		return n;
	}

	/**
	 * @param n the n to set
	 */
	public void setN(int n) {
		this.n = n;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attribute == null) ? 0 : attribute.hashCode());
		long temp;
		temp = Double.doubleToLongBits(gain);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + n;
		result = prime * result + p;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Litteral other = (Litteral) obj;
		if (attribute == null) {
			if (other.attribute != null)
				return false;
		} else if (!attribute.equals(other.attribute))
			return false;
		if (Double.doubleToLongBits(gain) != Double.doubleToLongBits(other.gain))
			return false;
		if (n != other.n)
			return false;
		if (p != other.p)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Litteral [attribute=" + attribute + ", value=" + value + ", p=" + p + ", n=" + n + ", gain=" + gain
				+ "]";
	}
}

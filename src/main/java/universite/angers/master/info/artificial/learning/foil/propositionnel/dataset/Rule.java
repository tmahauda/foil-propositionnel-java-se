package universite.angers.master.info.artificial.learning.foil.propositionnel.dataset;

import java.util.List;
import org.apache.log4j.Logger;

/**
 * Une règle est formé de prémisses (conditions) et d'une conclusion
 * SI prémisses ALORS condition
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class Rule {
	
	private static final Logger LOG = Logger.getLogger(Rule.class);
	
	/**
	 * L'ensemble des conditions formés par les littéraux
	 */
	private List<Litteral> conditions;
	
	/**
	 * La conclusion qui la classe cible
	 */
	private ClasseWrapper conclusion;
	
	/**
	 * Nombre d'instances positives qui vérifie la règle
	 */
	private int countInstancesPosCheckRule;
	
	/**
	 * Proportion d'instances positives qui vérifie la règle sur l'ensemble des instancances disponibles
	 */
	private double propInstancesPosCheckRule;
	
	/**
	 * Nombre d'instances négatives qui vérifie la règle
	 */
	private int countInstancesNegCheckRule;
	
	/**
	 * Proportion d'instances négative qui vérifie la règle sur l'ensemble des instancances disponibles
	 */
	private double propInstancesNegCheckRule;
	
	public Rule(List<Litteral> conditions, ClasseWrapper conclusion) {
		this.conditions = conditions;
		this.conclusion = conclusion;
	}

	/**
	 * Savoir si un exemple respecte toutes les conditions de la regle
	 * @param instance
	 * @return
	 */
	public boolean isRespect(InstanceWrapper instance) {
		LOG.debug("Instance : " + instance);
		
		for(Litteral l : this.conditions) {
			LOG.debug("Litteral : " + l);
			
			String valueLitteral = l.getValue();
			LOG.debug("Value litteral : " + valueLitteral);
			
			String valueInstance = instance.getValue(l.getAttribute());
			LOG.debug("Value instance : " + valueInstance);
			
			boolean equals = valueLitteral.equals(valueInstance);
			LOG.debug("Equals : " + equals);
			
			if(!equals) return false;
		}
		
		return true;
	}
	
	/**
	 * @return the conditions
	 */
	public List<Litteral> getConditions() {
		return conditions;
	}

	/**
	 * @param conditions the conditions to set
	 */
	public void setConditions(List<Litteral> conditions) {
		this.conditions = conditions;
	}
	
	/**
	 * @return the conclusion
	 */
	public ClasseWrapper getConclusion() {
		return conclusion;
	}

	/**
	 * @param conclusion the conclusion to set
	 */
	public void setConclusion(ClasseWrapper conclusion) {
		this.conclusion = conclusion;
	}

	/**
	 * @return the countInstancesPosCheckRule
	 */
	public int getCountInstancesPosCheckRule() {
		return countInstancesPosCheckRule;
	}

	/**
	 * @param countInstancesPosCheckRule the countInstancesPosCheckRule to set
	 */
	public void setCountInstancesPosCheckRule(int countInstancesPosCheckRule) {
		this.countInstancesPosCheckRule = countInstancesPosCheckRule;
	}

	/**
	 * @return the propInstancesPosCheckRule
	 */
	public double getPropInstancesPosCheckRule() {
		return propInstancesPosCheckRule;
	}

	/**
	 * @param propInstancesPosCheckRule the propInstancesPosCheckRule to set
	 */
	public void setPropInstancesPosCheckRule(double propInstancesPosCheckRule) {
		this.propInstancesPosCheckRule = propInstancesPosCheckRule;
	}

	/**
	 * @return the countInstancesNegCheckRule
	 */
	public int getCountInstancesNegCheckRule() {
		return countInstancesNegCheckRule;
	}

	/**
	 * @param countInstancesNegCheckRule the countInstancesNegCheckRule to set
	 */
	public void setCountInstancesNegCheckRule(int countInstancesNegCheckRule) {
		this.countInstancesNegCheckRule = countInstancesNegCheckRule;
	}

	/**
	 * @return the propInstancesNegCheckRule
	 */
	public double getPropInstancesNegCheckRule() {
		return propInstancesNegCheckRule;
	}

	/**
	 * @param propInstancesNegCheckRule the propInstancesNegCheckRule to set
	 */
	public void setPropInstancesNegCheckRule(double propInstancesNegCheckRule) {
		this.propInstancesNegCheckRule = propInstancesNegCheckRule;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((conditions == null) ? 0 : conditions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rule other = (Rule) obj;
		if (conditions == null) {
			if (other.conditions != null)
				return false;
		} else if (!conditions.equals(other.conditions))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for(Litteral l : this.conditions) {
			sb.append("SI ");
			sb.append(l.getAttribute().name());
			sb.append(" = ");
			sb.append(l.getValue());
			sb.append(" ET");
			sb.append(System.lineSeparator());
		}
		
		//On supprime le dernier ET
		sb.delete(sb.length()-3, sb.length());
		
		return sb.toString();
	}
}

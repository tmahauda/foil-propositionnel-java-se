package universite.angers.master.info.artificial.learning.foil.propositionnel;

import java.util.List;
import org.apache.log4j.Logger;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.DataSetWrapper;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.Evaluable;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.FactoryDataSet;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.InstanceWrapper;
import universite.angers.master.info.artificial.learning.foil.propositionnel.dataset.Rule;

/**
 * Test sur des jeux données
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 06/04/2020
 * @author Théo MAHAUDA, Mohamed OUHIRRA
 * @version 1.0
 */
public class Test {

	private static final Logger LOG = Logger.getLogger(Test.class);
	
	public static void main(String[] args) {

		/**
		 * FOILP1 ; WEATHER ; SPORT = YES => Temps de résolution : 138ms
		 */
		findRules(new FoilPropositionnelFOILP1(), "src/main/resources/data/weather_nominal.arff", "yes");
		
		/**
		 * FOILP1 ; SOLEIL ; COUP = OUI => Temps de résolution : 39ms
		 */
		//findRules(new FoilPropositionnelFOILP1(), "src/main/resources/data/coup_de_soleil.arff", "non");
		
		/**
		 * FOILP1 ; TIC TAC TOE ; GAGNE = POSITIVE => Temps de résolution : 162067ms = 3min
		 */
		//findRules(new FoilPropositionnelFOILP1(), "src/main/resources/data/tic_tac_toe.arff", "positive");
		
		/**
		 * FOILP1 ; LENTILLES ; PORTER = FORT => Temps de résolution : 167ms
		 */
		//findRules(new FoilPropositionnelFOILP1(), "src/main/resources/data/contact_lenses.arff", "hard");
	}
	
	/**
	 * Trouver un ensemble de règles d'un jeu de données selon la stratégie de résolution
	 * @param fp la stratégie de résolution
	 * @param filename le jeu de données
	 * @param valuePositive la valeur cible permettant d'évaluer les instances positives
	 * @return
	 */
	private static void findRules(FoilPropositionnel fp, String filename, String valuePositive) {
		DataSetWrapper dataSet = FactoryDataSet.buildAllData(filename, new Evaluable() {
			@Override
			public boolean isPositive(InstanceWrapper instance) {
				String value = instance.getValueClasse();
				LOG.debug("Value : " + value);
				
				return valuePositive.equals(value);
			}
		});
		
		if(dataSet == null) return;
		
		long start = System.currentTimeMillis();
		List<Rule> rules = fp.solve(dataSet);
		long end = System.currentTimeMillis();
		LOG.debug("Rules : " + rules);
		
		System.out.println("Données : " + filename);
		System.out.println("Temps de résolution (en ms) : " + (end-start));
		System.out.println("Les règles apprises : ");
		int i = 1;
		
		for(Rule rule : rules) {
			System.out.println();
			System.out.println("Regle : " + i);
			System.out.println(rule);
			System.out.println("ALORS " + dataSet.getData().classAttribute().name() + " = " + valuePositive);
			i++;
		}
	}

}
